process.env.NODE_ENV = 'test';
const CONFIG = {
  HOST: 'localhost:8080',

  validUser: {
    login: 'otest@yandex.ru',
    password: 'test123'
  },

  registerUser: {
    login: 'olroma123@yandex.ru',
    email: 'olroma123@yandex.ru',
    password: 'Morozov12345',
    role: 'Пользователь',
    firstname: 'Олег',
    surname: 'ТестТеста'
  },

  invalidUser: {
    login: 'asd',
    password: 'asd'
  },

  adminUser: {
    login: 'otest@yandex.ru',
    password: 'test123',
    token: '5a97edc230f99b0d48f7a788'
  }
};

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const queryString = require('query-string');

chai.use(chaiHttp);

describe('Register user', () => {
  it('Register new user success', done => {
    chai.request(CONFIG.HOST)
      .post('/user').set('content-type', 'application/x-www-form-urlencoded')
      .send(CONFIG.registerUser)
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        done();
      });
  });

  it('Register same user', done => {
    chai.request(CONFIG.HOST)
      .post('/user').set('content-type', 'application/x-www-form-urlencoded')
      .send(CONFIG.registerUser)
      .end((err, result) => {
        result.should.have.status(409);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        result.body.message.should.have.property('message').eql('Пользователь был найден в бд!');
        done();
      });
  });

  it('Checking for empty field', done => {
    chai.request(CONFIG.HOST)
      .post('/user').set('content-type', 'application/x-www-form-urlencoded')
      .send({
        login: CONFIG.registerUser.email,
        email: CONFIG.registerUser.email,
        password: CONFIG.registerUser.password,
        role: CONFIG.registerUser.role,
      })
      .end((err, result) => {
        result.should.have.status(400);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        result.body.message.should.have.property('message').eql('Не все параметры были переданы!');
        done();
      });
  });

  it('Restrict for admin role register', done => {
    chai.request(CONFIG.HOST)
      .post('/user').set('content-type', 'application/x-www-form-urlencoded')
      .send({
        login: CONFIG.registerUser.email,
        email: CONFIG.registerUser.email,
        password: CONFIG.registerUser.password,
        firstname: CONFIG.registerUser.firstname,
        surname: CONFIG.registerUser.surname,
        role: 'Администратор',
      })
      .end((err, result) => {
        result.should.have.status(403);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        result.body.message.should.have.property('message').eql('Отказано в доступе!');
        done();
      });
  });
});

describe('Auth user', () => {
  it('User is not confirm register by e-mail and admin', done => {

  });

  it('User is not confirmed by admin', done => {

  });

  it('Success auth user', done => {

  });

  it('Wrong user password', done => {

  });
});

describe('Reset password', () => {
  it('Success reset password', done => {

  });

  it('Trying to reset a password on e-mail, which not registered', done => {

  });

  it('Trying to reset a password, with 2 different password', done => {

  });
});

describe('GET /user', () => {
  it('should get user info', (done) => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?' + queryString.stringify(CONFIG.validUser))
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        done();
    });
  });

  it('should not GET an user info with wrong creditional', (done) => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?' + queryString.stringify(CONFIG.invalidUser))
      .end((err, result) => {
        result.should.have.status(401);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        done();
      });
  });

  it('should not GET an user info with empty param', (done) => {
    chai.request(CONFIG.HOST)
      .get('/user/auth')
      .end((err, result) => {
        result.should.have.status(400);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        done();
      });
  });

  it('should not GET an user info with wrong param', (done) => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?asdasd=asdsd')
      .end((err, result) => {
        result.should.have.status(400);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('error');
        done();
      });
  });
});