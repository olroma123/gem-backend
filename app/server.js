// =============== REQUIRE LIBS =================
const express     = require('express');
const app         = express();
const http        = require('http');
const logger      = require('log4js');
const log         = logger.getLogger('SERVER');
const MongoClient = require('mongodb').MongoClient;
const bodyParser  = require('body-parser');
const config      = require('./config');
const io          = require('socket.io')(config.setting.socket.port, {path: config.setting.socket.subURL});
logger.configure(config.log);
// ==============================================

// ================ DATABASE CONNECT ===============
log.info('Подключение к бд!');
db = MongoClient.connect(config.database.url)
  .then(db => start(db))
  .catch(err => log.fatal(`Возникла ошибка при подключении к бд! ${err}`));
// =========================================

// ================ APP USE =================
app.use(bodyParser.json({limit: '1000mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '1000mb'}));
//app.use(bodyParser.json({limit: '50mb'}));
app.use('/images', express.static(__dirname + '/static/img'));
app.use('/avatar', express.static(__dirname + '/static/avatar'));
app.use('/document', express.static(__dirname + '/static/medicalDocuments'));
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
// ==========================================


// ===================== SERVER ======================
function start(db) {
  let database = db.db('gem');
  log.info('Подключение к бд произошло успешно!');

  http.createServer(app).listen(config.setting.server.port, () => {
    // Require some modules for server
    require('./api')(app, {log: require('log4js').getLogger('API'), db: database, io: io, config: config});
    log.info('Сервер успешно запущен! Порт: ', config.setting.server.port);
  });
}
// ===================================================