module.exports = (app, lib) => {
  const Forum = require('./forum');
  const route = new Forum(lib);

  app.use('/forum', (req, res, next) => {
    lib.log.info(`URL: /forum${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Get a full structure of forum with count theme
  app.get('/forum/structure', (req, res) => route.getFullStructureForum(req, res));

  // Get topics by theme id
  app.get('/forum', (req, res) => route.getTopicsFromThemeId(req, res));

  // Get mini structure of global themes
  app.get('/forum/mini-structure', (req, res) => route.getMiniStructureOfRootTheme(req, res));

  // Get topic details by id
  app.get('/forum/topic', (req, res) => route.getTopicDetails(req, res));

  // Get user count created topics
  app.get('/forum/user-count/:userID', (req, res) => route.getUserCountThemes(req, res));

  // Get theme of forum details
  app.get('/forum/theme-details', (req, res) => route.getThemeDetails(req, res));

  // Get comments by count and offset
  app.get('/forum/comments-only', (req, res) => route.getCommentsOfTopicByOffset(req, res));

  // Adding a comment to topic by id
  app.post('/forum/comment/:token', (req, res) => route.addCommentTopic(req, res));

  // Add a new topic into theme by id
  app.post('/forum/:token', (req, res) => route.addTopic(req, res));

  // ----------------- ADMIN ------------------
  // Add new global theme to structure
  app.patch('/forum/global-theme/:token', (req, res) => route.addGlobalThemeToStructure(req, res));

  // Add a new sub theme to structure
  app.patch('/forum/sub-theme/:token', (req, res) => route.addSubthemeToForum(req, res));

  // Remove themes and sub themes
  app.delete('/forum/theme/:token', (req, res) => route.removeSubThemeFromForum(req, res));
};
