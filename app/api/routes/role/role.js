class Role {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  getRoleText(request, response) {
    this.lib.db.collection('Roles').find({isTranslated: true}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else response.status(200).send(this.lib.APIMessage.CREATOR.createSuccessMessage('', item));
    });
  }
}

module.exports = Role;