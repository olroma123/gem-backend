module.exports = (app, lib) => {
  const Role = require('./role');
  const route = new Role(lib);

  app.use('/roles', (req, res, next) => {
    lib.log.info(`URL: /roles${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Routing
  // Get role text
  app.get('/roles', (req, res) => route.getRoleText(req, res));
};
