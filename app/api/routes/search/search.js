class Search {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  doSearch(request, response) {
    this.lib.log.trace('Method "doSearch" - вызван');
    if (!request.body.filter) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "doSearch" - параметры прошли валидацию');
    const filterOptions = JSON.parse(request.body.filter);

    // Validate filter options json
    if (!filterOptions) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    // Perform a query
    let queryArray = [];
    if (filterOptions.users) {
      // Querying an user
      queryArray.push(new Promise((resolve, reject) => {
        this.lib.request(`http://${this.helper.apiHost}/user/search?search=${encodeURIComponent(request.query.search)}`, (err, response, body) => {
          if (err) reject({status: 500, text: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
          if (response.statusCode !== 200) reject({status: 500, text: this.lib.APIMessage.ERROR.SERVER_ERROR(body)});
          else resolve(JSON.parse(body));
        });
      }));
    } if (filterOptions.news) {
      queryArray.push(new Promise((resolve, reject) => {
        this.lib.request(`http://${this.helper.apiHost}/news/search?search=${encodeURIComponent(request.query.search)}`, (err, response, body) => {
          if (err) reject({status: 500, text: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
          if (response.statusCode !== 200) reject({status: 500, text: this.lib.APIMessage.ERROR.SERVER_ERROR(body)});
          else {
            resolve(JSON.parse(body));
          }
        });
      }));
    } if (filterOptions.posts) {

    }

    Promise.all(queryArray).then(values => {
      // Getting a values
      // And send to client a result
      values = values.map(value => value.data);
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(values));
    }).catch(error => {
      response.status(error.status).send(error.text);
    });
  }
}

module.exports = Search;
