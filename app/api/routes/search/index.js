module.exports = (app, lib) => {
  const Search = require('./search');
  const route = new Search(lib);

  app.use('/search', (req, res, next) => {
    lib.log.info(`URL: /search${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  app.post('/search', (req, res) => route.doSearch(req, res));
};
