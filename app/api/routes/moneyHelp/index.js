module.exports = (app, lib) => {
  const MoneyHelp = require('./moneyHelp');
  const route = new MoneyHelp(lib);

  app.use('/money-help', (req, res, next) => {
    lib.log.info(`URL: /money-help${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Get money-help items
  app.get('/money-help', (req, res) => route.getMoneyHelp(req, res))
};
