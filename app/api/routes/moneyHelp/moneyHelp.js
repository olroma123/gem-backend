class MoneyHelp {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  getMoneyHelp(request, response) {
    this.lib.log.trace('Method "getMoneyHelp" - вызыван');
    this.lib.db.collection('money-help').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null || item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.MONEY_HELP_EMPTY());
      else {
        let moneyHelpInfo = item;
        for (let i = 0; i < moneyHelpInfo.length; i++)
          moneyHelpInfo[i].img = `http://${this.helper.apiHost}/images/money-help/${moneyHelpInfo[i]._id}.jpg`;
        if (request.query.count) {
          let tmp = [];
          for (let i = 0; i < request.query.count; i++) {
            tmp.push(this.lib.randomItem(moneyHelpInfo));
            tmp[i].readyPercent = Math.round(((tmp[i].total - tmp[i].remain) * 100) / tmp[i].total);
            moneyHelpInfo = moneyHelpInfo.filter((obj) => obj._id !== tmp[i]._id);
            if (moneyHelpInfo.length === 0) break;
          }
          moneyHelpInfo = tmp;
        }
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(moneyHelpInfo));
      }
    });
  }
}

module.exports = MoneyHelp;
