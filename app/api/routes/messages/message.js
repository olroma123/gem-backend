class Message {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
    this.getTemplateMessage = (messageDb, userIDSender) => {
      return {
        message: {
          id: messageDb.message.messageID,
          date: messageDb.message.lastMessageDate,
          text: (messageDb.message.fromUserID === userIDSender ? 'Вы: ' : '') + messageDb.message.messageText,
          avatar: this.helper.getUserAvatarByID(messageDb.message.fromUserID === userIDSender ? messageDb.message.toUserID : messageDb.message.userID || messageDb.message.fromUserID),
          fromUser: messageDb.message.fromUserID === userIDSender ? messageDb.message.fromUser : messageDb.message.toUser,
          toUser: messageDb.message.fromUserID === userIDSender ? messageDb.message.toUser : messageDb.message.fromUser,
          userID: (messageDb.message.fromUserID === userIDSender ? messageDb.message.toUserID : messageDb.message.userID || messageDb.message.fromUserID)
        }
      }
    };

    this.getTemplateMessageTemp = (messageDb, userIDSender) => {
      return {
        message: {
          id: messageDb.message.id || messageDb.message._id,
          date: messageDb.message.date,
          text: (messageDb.message.from === userIDSender ? 'Вы: ' : '') + messageDb.message.text,
          avatar: this.helper.getUserAvatarByID(messageDb.message.from === userIDSender ? messageDb.message.to : messageDb.message.from),
          fromUser: messageDb.message.from === userIDSender ? messageDb.message.fullNameFrom : messageDb.message.fullNameTo,
          toUser: messageDb.message.from === userIDSender ? messageDb.message.fullNameTo : messageDb.message.fullNameFrom,
          userID: (messageDb.message.from === userIDSender ? messageDb.message.to : messageDb.message.from)
        }
      }
    };

    this.preloadMessage = (message) => {
      return {
        message: {
          id: message.messageID,
          date: message.lastMessageDate,
          text: message.messageText,
          fromUser: message.fullNameFrom,
          toUser: message.fullNameTo,
          fromUserID: message._id,
          toUserID: message.to,
          userID: message.to,
          status: message.status,
          avatar: this.helper.getUserAvatarByID(message.to)
        }
      };
    };
  }

  /**
  * Отправка сообщения пользователю по его email
  */
  sendMessageToUser(request, response) {
    this.lib.log.trace(`Method "sendMessageToID" - вызыван`);
    if (!request.body.toUser || !request.body.fromUser || !request.body.textMessage) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "sendMessageToID" - прошел валидацию параметров`);

    let messageInfo = {
      to: request.body.toUser,
      from: request.body.fromUser,
      text: this.lib.stripTags(request.body.textMessage),
      status: 'pending'
    };
    this.lib.log.trace(`Method "sendMessageToID" - сообщение от ${messageInfo.from} --> ${messageInfo.to}`);
    this.lib.db.collection('users').find({$or: [{_id: this.lib.ObjectID(messageInfo.from)}, {id: messageInfo.to}]}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null || item.length < 2) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        // Check for register status
        if (this.helper.userIsNotConfirmed(item[0]) || this.helper.userIsNotConfirmed(item[1])) {
          response.status(423).send(this.lib.APIMessage.CREATOR.createErrorMessage('Один из пользователей не подтвержден!'));
          return;
        } else if (item[1].privacy.allowMessages === 'false') {
          response.status(412).send(this.lib.APIMessage.CREATOR.createErrorMessage('Отправка сообщений этому пользоватлю запрещена настройками приватности'));
          return;
        }

        messageInfo.from = item[0].id === request.body.toUser ? item[1].id : item[0].id;
        let toUser = item[0].id === request.body.toUser ? item[0] : item[1];
        let fromUser = item[0].id === request.body.toUser ? item[1] : item[0];
        messageInfo.fullNameFrom = `${fromUser.public.fullName.surname} ${fromUser.public.fullName.name}`;
        messageInfo.fullNameTo = `${toUser.public.fullName.surname} ${toUser.public.fullName.name}`;
        messageInfo.date = new Date();
        this.lib.log.trace(`Method "sendMessageToID" - отправка сообщения...`);
        this.lib.db.collection('messages').insert(messageInfo, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else {
            this.lib.log.trace(`Method "sendMessageToID" - успешно`);

            // Send message info to client throw sockets
            this.helper.sendDataToSocketID(toUser._id, {
              event: 'new message',
              message: {
                text: messageInfo.text,
                date: messageInfo.date,
                status: 'pending',
                fullNameFrom: `${fromUser.public.fullName.surname} ${fromUser.public.fullName.name}`,
                fullNameTo: `${toUser.public.fullName.surname} ${toUser.public.fullName.name}`
              }
            });

            // Send a notification data to clients
            this.lib.request(`http://${this.helper.apiHost}/notification/${request.body.fromUser}`, (err, response, body) => {
              const answer = JSON.parse(body);
              if (err) this.lib.log.error(err);
              else if (response.statusCode === 200) {
                this.helper.sendDataToSocketID(toUser._id, {
                  event: 'notification',
                  message: {
                    message: answer.message,
                    notification: answer.data
                  }
                });
              }
            });
            response.send(this.lib.APIMessage.SUCCESS.MESSAGE_SEND_SUCCESS());
          }
        });
      }
    });
  }

  /**
  * Метод для получения сообщения по ID
  */
  getMessageByID(request, response) {
    this.lib.log.trace(`Method "getMessageByID" - вызыван`);
    if (!request.params.messageID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "getMessageByID" - прошел валидацию параметров`);

    this.lib.db.collection('messages').findOne({'_id': new this.lib.ObjectID(request.params.messageID)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else response.status(200).send(this.lib.APIMessage.CREATOR.createSuccessMessage('', item));
    });
  }

  /**
  * Метод для получения сообщений пользователя
  */
  getMessagesBySender(request, response) {
    this.lib.log.trace(`Method "getMessageBySenderID" - вызыван`);
    if (!request.params.sender || !request.params.recipient) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace(`Method "getMessagesBySenderID" - прошел валидацию параметров`);
    this.helper.searchUserByToken(request.params.sender).then(item => {
      let messages = [
        {$lookup: {from: "users", localField: "from", foreignField: "id", as: "userInfo"}},
        {$match: {$or: [{to: request.params.recipient, from: item.id}, {to: item.id, from: request.params.recipient}]}},
        {$sort: {date: 1}}
      ];
      this.lib.db.collection('messages').aggregate(messages).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null || item.length === 0) {
          this.lib.db.collection('users').findOne({id: request.params.recipient}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else if (item === null) response.status(410).send(this.lib.APIMessage.ERROR.USER_NOT_FOUND());
            else response.status(412).send(this.lib.APIMessage.CREATOR.createSuccessMessage('Нет сообщений', {
                userFullName: `${item.public.fullName.surname} ${item.public.fullName.name}`,
                avatar: this.helper.getUserAvatarByID(item.id)
              }));
          });
        }
        else {
          let messages = {
            userFullName: undefined,
            avatar: undefined,
            messages: []
          };
          messages.messages = item.map(msg => {
            if (!messages.avatar) {
              messages.userFullName = msg.to === request.params.recipient ? msg.fullNameTo : msg.fullNameFrom;
              messages.avatar = this.helper.getUserAvatarByID(request.params.recipient);
            }

            return {
              text: msg.text,
              date: msg.date,
              status: msg.status,
              userFullName: msg.userInfo[0] === undefined ? msg.fullNameFrom : this.helper.getFullName(msg.userInfo[0].public.fullName),
              userID: msg.userInfo[0] === undefined ? msg.from : msg.userInfo[0].id,
              avatar: this.helper.getUserAvatarByID(msg.userInfo[0] === undefined ? msg.from : msg.userInfo[0].id)
            }
          });
          response.status(200).send(this.lib.APIMessage.CREATOR.createSuccessMessage('', messages));
          this.lib.request({
            uri: `http://${this.helper.apiHost}/message/${request.params.sender}/read/${request.params.recipient}`,
            method: 'PATCH'
          }, (err, response, body) => {
            if (err) this.lib.log.error(err);
            if (response.statusCode !== 200) this.lib.log.debug(body);
            else {
              this.lib.request(`http://${this.helper.apiHost}/notification/${request.params.sender}`, (err, response, body) => {
                const answer = JSON.parse(body);
                if (err) this.lib.log.error(err);
                else if (response.statusCode === 200)
                  this.helper.sendDataToSocketID(request.params.sender, {event: 'notification', message: {message: answer.message, notification: answer.data}});
              });
            }
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
  * Метод для изменения статуса сообщения
  */
  readMessage(request, response) {
    this.lib.log.trace(`Method "readMessage" - вызыван`);
    if (!request.params.messageID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "readMessage" - прошел валидацию параметров`);
    this.lib.db.collection('messages').updateOne({'_id': new this.lib.ObjectID(request.params.messageID)}, {$set: {status: 'read'}}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else response.status(200).send(this.lib.APIMessage.CREATOR.createSuccessMessage(''));
    });
  }

  /**
   * Метод для изменения статуса всех сообщений от пользователя на "прочитано"
   **/
  readAllMessageFromUser(request, response) {
    this.lib.log.trace(`Method "readAllMessageFromUser" - вызыван`);
    if (!request.params.token || !request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "readAllMessageFromUser" - прошел валидацию параметров`);
    this.helper.searchUserByToken(request.params.token).then(item => {
      this.lib.db.collection('messages').updateMany({$and: [{from: request.params.userID}, {to: item.id}]}, {$set: {status: 'read'}}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else response.status(200).send(this.lib.APIMessage.CREATOR.createSuccessMessage(''));
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
  * Метод для получения последних сообщений
  */
  getLastMessages(request, response) {
    this.lib.log.trace(`Method "getLastMessages" - вызыван`);
    if (!request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "getLastMessages" - прошел валидацию параметров`);

    // Validate user token
    this.helper.searchUserByToken(request.params.userID)
      .then(user => {
        this.lib.db.collection('messages').find({from: user.id}).toArray((err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));

          // If we not have a messages that user sent
          // Then we need to check a messages, that sent to user
          else if (item.length === 0) {
            this.lib.db.collection('messages').find({to: user.id}).toArray((err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));

              // If we still not found a message
              // Then user not have a message. We can just send EMPTY_MESSAGE to user
              else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());

              // If we found a messages
              // Then need do some action on it
              else if (item.length > 0) {
                // Find last message
                let messages = [];
                for (let i = 0; i < item.length; i++) {
                  let filterMessage = messages.findIndex(message => message.from === item[i].from && message.to === item[i].to);
                  if (filterMessage === -1) messages.push(item[i]);
                  else {
                    if (new Date(messages[filterMessage].date) > new Date(item[i].date))
                      messages[filterMessage] = item[i];
                  }
                }
                messages = messages.map(message => this.getTemplateMessageTemp({message: message}, user.id));
                response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(messages));
              }
            });
          } else if (item.length > 0) {
            let saveItem = item;
            // Now we need to check if TO user is exists but not exists in our array
            this.lib.db.collection('messages').find({to: user.id}).toArray((err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
              else if (item.length > 0) {
                for (let i = 0; i < item.length; i++) {
                  let isFind = false;
                  for (let j = 0; j < saveItem.length; j++) {
                    if (item[i].to === user.id) {
                      for (let k = 0; k < saveItem.length; k++) {
                        if (item[i].from === saveItem[k].to) {
                          isFind = true;
                          break;
                        }
                      }
                      if (isFind) break;
                    }
                  }
                  if (!isFind) saveItem.push(item[i]);
                }
              }
              // Remove duplicates and sort by date
              item = saveItem;
              let messages = [];
              for (let i = 0; i < item.length; i++) {
                let filterMessage = messages.findIndex(message => message.from === item[i].from && message.to === item[i].to);
                if (filterMessage === -1) messages.push(item[i]);
                else {
                  if (new Date(messages[filterMessage].date) < new Date(item[i].date))
                    messages[filterMessage] = item[i];
                }
              }
              let waitLoaded = [];
              for (let i = 0; i < messages.length; i++) {
                let saveItem = messages[i];
                waitLoaded.push(new Promise((resolve, reject) => {
                  let lastMessageToQuery = [
                    {$match: {$and: [{from: messages[i].to}, {to: user.id}]}},
                    {$group: {
                        _id: "$from",
                        lastMessageDate: {$last: "$date"},
                        messageText: {$last: "$text"},
                        messageID: {$last: "$_id"},
                        toUserID: {$last: "$to"},
                        fromUserID: {$last: "$from"},
                        status: {$last: "$status"},
                        user: {$last: "$userInfo"},
                        fromUser: {$last: "$fullNameFrom"},
                        toUser: {$last: "$fullNameTo"}
                      }
                    }
                  ];
                  this.lib.db.collection('messages').aggregate(lastMessageToQuery).toArray((err, item) => {
                    if (err) reject({status: 500, message: err});
                    else if (item.length === 0) resolve(this.getTemplateMessageTemp({message: saveItem}, user.id));
                    else if (item.length > 0) {
                      if (new Date(saveItem.date) < new Date(item[0].lastMessageDate)) {
                        resolve(this.getTemplateMessage({message: item[0]}, user.id));
                      } else {
                        resolve(this.getTemplateMessageTemp({message: saveItem}, user.id));
                      }
                    }
                  });
                }));
              }

              Promise.all(waitLoaded).then(values => {
                this.lib.log.trace(`Method "getLastMessages" - получено сообщений: ${values.length}`);
                response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(values));
              }).catch(err => {
                response.status(err.status).send(err.message);
              })
            });
          }
        });
      });
  }
}

module.exports = Message;
