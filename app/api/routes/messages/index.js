module.exports = (app, lib) => {
  const Message = require('./message');
  const route = new Message(lib);

  app.use('/message', (req, res, next) => {
    lib.log.info(`URL: /message${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Routing
  // Send message
  app.post('/message', (req, res) => route.sendMessageToUser(req, res));
  // Get message by id
  app.get('/message/:messageID', (req, res) => route.getMessageByID(req, res));
  // Get last messages
  app.get('/message/last/:userID', (req, res) => route.getLastMessages(req, res));
  // Get message by from
  app.get('/message/from/:sender/to/:recipient', (req, res) => route.getMessagesBySender(req, res));
  // Update message status
  app.patch('/message/read/:messageID', (req, res) => route.readMessage(req, res));
  // Update message status from user
  app.patch('/message/:token/read/:userID', (req, res) => route.readAllMessageFromUser(req, res));
};
