class Library {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для добавления раздела
   * */
  addFolderToLibrary(request, response) {
    this.lib.log.trace(`Method "addFolderToLibrary" - вызыван`);
    if (!request.params.token || !request.body.title || !request.query.parentId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "addFolderToLibrary" - прошел валидацию параметров`);
    request.body.title = this.lib.stripTags(request.body.title);
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      if (request.query.parentId !== '/') {
        this.lib.db.collection('library').findOne({_id: this.lib.ObjectID(request.query.parentId)}, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item === null) response.status(412).send(this.lib.APIMessage.ERROR.LIBRARY_ID_NOT_FOUND());
          else {
            if (item.isBook) {
              response.status(412).send(this.lib.APIMessage.ERROR.CANT_ADD_FOLDER_TO_BOOK_LIB());
              return;
            }

            const folder = {
              isBook: false,
              name: request.body.title,
              parent: item._id.toString()
            };

            this.lib.db.collection('library').insertOne(folder, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id.toString()}));
            });
          }
        });
      } else {
        const folder = {
          isBook: false,
          name: request.body.title,
          parent: '/'
        };

        this.lib.db.collection('library').insertOne(folder, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id.toString()}));
        });
      }
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения всех родительских "папок"
   * */
  getAllParentsFolder(request, response) {
    this.lib.log.trace(`Method "getAllParentsFolder" - вызыван`);
    this.lib.db.collection('library').find({isBook: false}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
    });
  }

  /**
   * Метод для получения всех статей
   * */
  getAllBooks(request, response) {
    this.lib.log.trace(`Method "getAllParentsFolder" - вызыван`);
    this.lib.db.collection('library').find({isBook: true}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
    });
  }

  /**
   * Метод для удаления раздела/подразделов/статей
   * */
  removeLib(request, response) {
    const getChildrenList = (id, structure) => {
      // Фильтруем данные по id родителя
      let children = structure.filter(folder => folder.parent === id), list = [];

      for (let i = 0; i < children.length; i++) {
        if (children[i].isBook) list.push(children[i]);
        else {
          list.push(children[i]);
          let childrenArray = getChildrenList(children[i]._id.toString(), structure);
          if (childrenArray.length > 0) list.push(childrenArray)
        }
      }

      return list;
    };

    this.lib.log.trace(`Method "removeLib" - вызыван`);
    if (!request.query.id || !request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "removeLib" - прошел валидацию параметров`);

    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      this.lib.db.collection('library').findOne({_id: this.lib.ObjectID(request.query.id)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(412).send(this.lib.APIMessage.ERROR.LIBRARY_ID_NOT_FOUND());
        else {
          // If query id is just simple book
          // Then we need to delete this
          if (item.isBook) {
            this.lib.db.collection('library').deleteOne({_id: this.lib.ObjectID(request.query.id)}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else {
                let picture = `./app/static/img/library/${request.query.id}.jpg`;
                this.lib.stat(picture, (err, stat) => {
                  if (!err) this.lib.fs.unlinkSync(picture);
                });

                response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
              }
            });
          } else {
            // Else if id is folder
            // Than we need to get deeper id and delete it
            this.lib.db.collection('library').find({}).toArray((err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else if (item.length === 0) response.status(204).send();
              else {
                let listOfIds = getChildrenList(request.query.id, item), waiting = [],
                  indexOfQueryTheme = item.findIndex(folder => folder._id.toString() === request.query.id);
                if (indexOfQueryTheme !== -1) listOfIds.unshift(item[indexOfQueryTheme]);

                for (let i = 0; i < listOfIds.length; i++) {
                  waiting.push(new Promise((resolve, reject) => {
                    this.lib.db.collection('library').deleteOne({_id: this.lib.ObjectID(listOfIds[i]._id)}, (err, item) => {
                      if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                      else {
                        if (listOfIds[i].isBook)
                          this.lib.fs.unlinkSync(`./app/static/img/library/${listOfIds[i]._id}.jpg`);
                        resolve();
                      }
                    });
                  }));
                }

                Promise.all(waiting).then(() => {
                  response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
                }).catch(err => {
                  response.status(err.status).send(err.message);
                });
              }
            });
          }
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для получения статьи по id
   * */
  getBookById(request, response) {
    this.lib.log.trace(`Method "getBookById" - вызыван`);
    if (!request.query.id) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "getBookById" - прошел валидацию параметров`);
    this.lib.db.collection('library').findOne({_id: this.lib.ObjectID(request.query.id)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item == null) response.status(412).send(this.lib.APIMessage.ERROR.LIBRARY_BOOK_ID_NOT_FOUND());
      else {
        if (!item.isBook) {
          response.status(412).send(this.lib.APIMessage.ERROR.LIBRARY_FOLDER_NOT_BOOK());
          return;
        }

        item.img = `http://${this.helper.apiHost}/images/library/${item._id.toString()}.jpg`;
        response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
      }
    });
  }

  /**
   * Метод для добавления статьи для раздела
   * */
  addBookToParent(request, response) {
    this.lib.log.trace(`Method "addBookToParent" - вызыван`);
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace(`Method "addBookToParent" - прошел валидацию параметров`);
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      const ID = this.lib.ObjectID();
      const storage = this.lib.multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, './app/static/img/library/')
        },
        filename: (req, file, cb) => {
          if (!file) {
            cb(this.lib.APIMessage.ERROR.LIBRARY_IMAGE_NEED(), false);
          }

          if (file.mimetype.includes('image')) {
            cb(null, `${ID.toString()}.jpg`);
          } else cb(this.lib.APIMessage.ERROR.WRONG_FILE_EXT('jpg, jpeg, png'), false);
        }
      });

      const upload = this.lib.multer({ storage: storage }).single('image');
      upload(request, response, (err) => {
        if (err) response.status(400).send(err);
        else {
          let info = this.helper.tryParseJson(this.lib.stripTags(request.body.info));
          if (!info || !request.body.parent) {
            response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
            return;
          }

          this.lib.db.collection('library').findOne({_id: this.lib.ObjectID(request.body.parent)}, (err, libFolder) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            else if (libFolder === null) response.status(412).send(this.lib.APIMessage.ERROR.LIBRARY_ID_NOT_FOUND());
            else {
              if (libFolder.isBook) {
                response.status(412).send(this.lib.APIMessage.ERROR.CANT_ADD_BOOK_TO_BOOK_LIB());
                return;
              }

              const lib = {
                _id: ID,
                isBook: true,
                name: info.title,
                text: info.text,
                parent: libFolder._id.toString()
              };

              this.lib.db.collection('library').insertOne(lib, (err, item) => {
                if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
                else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id}));
              });
            }
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения полной структуры библиотеки
   * */
  getStructureOfLibrary(request, response) {
    /**
     * Метод для получения "детей" для дерева
     * */
    const getChildren = (id, fullStructure) => {
      // Фильтруем данные по id родителя
      let children = fullStructure.filter(folder => folder.parent === id), tree = [];

      for (let i = 0; i < children.length; i++) {
        if (children[i].isBook) tree.push(children[i]);
        else tree.push(Object.assign({}, children[i], {children: getChildren(children[i]._id.toString(), fullStructure)}));
      }

      return tree;
    };

    this.lib.log.trace(`Method "getStructureOfLibrary" - вызыван`);
    this.lib.db.collection('library').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else {
        let globalThemes = item.filter(folder => folder.parent === '/'), tree = [], structure = item.filter(folder => folder.parent !== '/');
        for (let i = 0; i < globalThemes.length; i++) {
          const id = globalThemes[i]._id.toString();
          globalThemes[i].children = getChildren(id, structure);
        }

        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(globalThemes));
      }
    });
  }
}

module.exports = Library;
