module.exports = (app, lib) => {
  const Library = require('./library');
  const route = new Library(lib);

  app.use('/lib/', (req, res, next) => {
    lib.log.info(`URL: /lib${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // get a structure of library
  app.get('/lib/structure', (req, res) => route.getStructureOfLibrary(req, res));

  // get a full info about book by id
  app.get('/lib', (req, res) => route.getBookById(req, res));

  // get all folders
  app.get('/lib/folders', (req, res) => route.getAllParentsFolder(req, res));

  // get all books
  app.get('/lib/books', (req, res) => route.getAllBooks(req, res));

  // add book to folder
  app.post('/lib/folder/:token', (req, res) => route.addFolderToLibrary(req, res));

  // add folder
  app.post('/lib/book/:token', (req, res) => route.addBookToParent(req, res));

  // delete lib's book or folder by id
  app.delete('/lib/:token', (req, res) => route.removeLib(req, res));
};
