class Post {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  addPost(request, response) {
    this.lib.log.trace('Method "addPost" - вызван');
    if (!request.params.token || !request.body.post || typeof request.body.post !== 'string') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    let post = JSON.parse(request.body.post);

    // Validate post
    if (!post.text) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace('Method "addPost" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let postDate = new Date();
      post.date = postDate;
      post.userID = user.id;
      post.comments = [];
      post.images = [];

      this.lib.db.collection('posts').insertOne(post, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else response.send(this.lib.APIMessage.SUCCESS.ADD_POST_SUCCESS());
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  addComment(request, response) {
    this.lib.log.trace('Method "addComment" - вызван');
    if (!request.params.token || !request.params.postId || !request.body.text) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addComment" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('posts').findOne({_id: this.lib.ObjectID(request.params.postId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null) response.status(412);
        else {
          let comments = item.comments;
          const userFullName = user.public.fullName;
          comments.push({
            id: user.id,
            fullName: `${userFullName.surname} ${userFullName.name}`,
            text: request.body.text.trim(),
            date: new Date().toISOString()
          });

          this.lib.db.collection('posts').updateMany({_id: this.lib.ObjectID(request.params.postId)}, {$set: {comments: comments}}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          });
        }
      });

    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения всех постов пользователей
   * */
  getPostsSortedByDate(request, response) {
    this.lib.log.trace('Method "getPostsSortedByDate" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getPostsSortedByDate" - прошел валидация параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let sortedPostQuery = [{$lookup: {from: "users", localField: "userID", foreignField: "id", as: "userInfo"}}];
      this.lib.db.collection('posts').aggregate(sortedPostQuery).sort({'date': -1}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item.length === 0) response.status(204);
        else {
          this.lib.request(`http://${this.helper.apiHost}/friend/${user._id}/accepted`, (err, res, body) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            if (response.statusCode !== 200) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(body));
            else {
              let friends = JSON.parse(res.body).data.friends;
              let posts = item.map(post => {
                let userFullName = post.userInfo[0].public.fullName;

                // Load image information to array
                let images = [];
                for (let i = 0; i < post.images.length; i++)
                  images.push(`http://${this.helper.apiHost}/images/posts/${post._id.toString()}/${post.images[i]}`);

                let comments = post.comments.map(comment => {
                  return {
                    id: comment.id,
                    fullName: comment.fullName,
                    text: comment.text,
                    date: comment.date,
                    avatar: this.helper.getUserAvatarByID(comment.id)
                  }
                });

                return {
                  user: {
                    fullName: `${userFullName.surname} ${userFullName.name}`,
                    id: post.userID,
                    avatar: `http://${this.helper.apiHost}/avatar/${post.userInfo[0].id}.jpg`
                  },
                  post: {
                    text: post.text,
                    date: post.date,
                    comments: comments.reverse(),
                    images: images,
                    id: post._id.toString(),
                    isSticky: post.isSticky || false
                  }
                }
              });
              // sort post by friends
              if (friends) {
                posts = posts.filter(post => {
                  for (let i = 0; i < friends.length; i++)
                    if (friends[i].id === post.user.id)
                      return true;
                  return false;
                });
              }

              if (posts.length === 0) response.status(204);
              else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(posts));
            }
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения постов пользователя через его userId
   * */
  getUserPostsSortedByDate(request, response) {
    this.lib.log.trace('Method "getUserPostsSortedByDate" - вызван');
    if (!request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUserPostsSortedByDate" - прошел валидация параметров');
    this.lib.db.collection('users').findOne({id: request.params.userID}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        const user = item;
        let sortedPostQuery = [
          {$lookup: {from: "users", localField: "userID", foreignField: "id", as: "userInfo"}},
          {$match: {userID: request.params.userID}}
        ];
        this.lib.db.collection('posts').aggregate(sortedPostQuery).sort({'date': -1}).toArray((err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.ADD_POST_EMPTY());
          else {
            let posts = item.map(post => {
              // Load image information to array
              let images = [];
              for (let i = 0; i < post.images.length; i++)
                images.push(`http://${this.helper.apiHost}/images/posts/${post._id.toString()}/${post.images[i]}`);

              // Getting avatar
              let comments = post.comments.map(comment => {
                return {
                  id: comment.id,
                  fullName: comment.fullName,
                  text: comment.text,
                  date: comment.date,
                  avatar: this.helper.getUserAvatarByID(comment.id)
                }
              });

              return {
                text: post.text,
                date: post.date,
                comments: comments.reverse(),
                images: images,
                id: post._id.toString(),
                isSticky: post.isSticky || false
              }
            });
            let sticky = posts.findIndex(post => post.isSticky === true);
            if (sticky !== -1) posts = require('array-move')(posts, sticky, 0);
            const userFullName = user.public.fullName;
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({
              user: {
                fullName: `${userFullName.surname} ${userFullName.name}`,
                id: user.id,
                avatar: this.helper.getUserAvatarByID(user.id)
              },
              posts: posts
            }));
          }
        });
      }
    });
  }

  /**
   * Метод для закрепления поста на стене пользователя
   * */
  fixPostOnUserWall(request, response) {
    this.lib.log.trace('Method "fixPostOnUserWall" - вызван');
    if (!request.params.token || !request.params.postId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "fixPostOnUserWall" - прошел валидация параметров');

    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('posts').findOne({_id: this.lib.ObjectID(request.params.postId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
        else {
          if (user.id !== item.userID) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
          else {
            let post = item;
            this.lib.db.collection('posts').updateMany({userID: user.id}, {$set: {isSticky: false}}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
              else {
                this.lib.db.collection('posts').updateOne({_id: this.lib.ObjectID(request.params.postId)}, {$set: {isSticky: !post.isSticky}}, (err, item) => {
                  if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
                  else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
                });
              }
            });
          }
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для удаления поста пользователя
   * */
  removePost(request, response) {
    this.lib.log.trace('Method "removePost" - вызван');
    if (!request.params.token || !request.query.postId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "removePost" - прошел валидация параметров');

    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('posts').findOne({_id: this.lib.ObjectID(request.query.postId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.POST_NOT_EXISTS());
        else {
          if (user.id !== item.userID) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
          else {
            this.lib.db.collection('posts').deleteOne({_id: this.lib.ObjectID(request.query.postId)}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
              else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
            });
          }
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения детальной информации о посте через его id
  */
  getPostDetailsById(request, response) {
    this.lib.log.trace('Method "getPostDetailsById" - вызван');
    if (!request.params.userId || !request.query.postId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getPostDetailsById" - прошел валидацию параметров');

    this.helper.searchUserByID(request.params.userId).then(user => {
      this.lib.db.collection('posts').findOne({_id: this.lib.ObjectID(request.query.postId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null) response.status(204);
        else {
          item.comments = item.comments.map(comment => {
            return {
              id: comment.id,
              fullName: comment.fullName,
              text: comment.text,
              date: comment.date,
              avatar: this.helper.getUserAvatarByID(comment.id)
            }
          });

          item.comments = item.comments.reverse();
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = Post;
