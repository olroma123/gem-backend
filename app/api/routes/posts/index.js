module.exports = (app, lib) => {
  const Post = require('./posts');
  const route = new Post(lib);

  app.use('/post', (req, res, next) => {
    lib.log.info(`URL: /post${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Add new post
  app.post('/post/:token', (req, res) => route.addPost(req, res));

  // Comment post
  app.patch('/post/:token/:postId', (req, res) => route.addComment(req, res));

  // Get posts sorted by date
  app.get('/post/:token/sortedByDate', (req, res) => route.getPostsSortedByDate(req, res));

  // Get posts by user id and date sorted
  app.get('/post/:userID', (req, res) => route.getUserPostsSortedByDate(req, res));

  // Get details info about post
  app.get('/post/:userId/details', (req, res) => route.getPostDetailsById(req, res));

  // Fix the post on wall
  app.patch('/post/:token/sticky/:postId', (req, res) => route.fixPostOnUserWall(req, res));

  // Delete post
  app.delete('/post/:token/', (req, res) => route.removePost(req, res));
};
