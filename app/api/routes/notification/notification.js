class Notification {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  getCountNotification(request, response) {
    this.lib.log.trace(`Method "getCountNotification" - вызыван`);
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace(`Method "getCountNotification" - прошел валидацию параметров`);

    let waitForChecking = [];

    // Load count of unread messages
    waitForChecking.push(new Promise((resolve, reject) => {
      this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.token)}, (err, item) => {
        if (err) reject(err);
        else if (item === null) reject('Отказано в доступе!');
        else {
          let lastMessageToQuery = [
            {$match: {$and: [{to: item.id}, {status: 'pending'}]}},
            {$group: {_id: "$from", lastMessageDate: {$last: "$date"}}},
            {$sort: {lastMessageDate: -1}}
          ];

          this.lib.db.collection('messages').aggregate(lastMessageToQuery).toArray((err, item) => {
            if (err) reject(err);
            else resolve(Object.assign({}, {messages: item.length}));
          });
        }
      });
    }));

    // Load count of requested friends
    waitForChecking.push(new Promise((resolve, reject) => {
      let requestedFriendQuery = [
        {$match: {$and: [{user2: this.lib.ObjectID(request.params.token)}, {status: "requested"}]}},
        {$group: { "_id": "$_id", user: {$last: "$userInfo"}}}
      ];

      this.lib.db.collection('friends').aggregate(requestedFriendQuery).toArray((err, item) => {
        if (err) reject(err);
        else resolve(Object.assign({}, {friends: item.length}));
      });
    }));

    // Wait all promises
    Promise.all(waitForChecking).then(notification => {
      // If success
      // Than send a notification info
      notification = {
        messages: notification[0].messages,
        friends: notification[1].friends
      };
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(notification));
    }, rejectReason => {
      // If not success
      // Than send an error to client
      response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${rejectReason}`));
    });
  }
}

module.exports = Notification;
