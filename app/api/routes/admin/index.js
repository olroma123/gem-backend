module.exports = (app, lib) => {
  const Admin = require('./admin');
  const route = new Admin(lib);

  app.use('/admin', (req, res, next) => {
    lib.log.info(`URL: /admin${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  app.use('/admin', (req, res, next) => {
    const token = req.url.split('/')[1];
    lib.helper.searchUserByTokenAndValidateAdminRights(token).then(() => next()).catch(err => {
      res.status(err.status).send(err.message);
      lib.log.error(`Произошла ошибка при авторизации администратора!`);
    });
  });

  // Confirm user register
  app.patch('/admin/:token/regConfirm', (req, res) => route.confirmUserRegister(req, res));

  // Update user role
  app.patch('/admin/:token/role', (req, res) => route.changeUserRole(req, res));

  // Delete user
  app.delete('/admin/:token/removeUser', (req, res) => route.deleteUser(req, res));

  // Deactivate user
  app.patch('/admin/:token/deactivate', (req, res) => route.deactivateUser(req, res));

  // Get users that need to moderate
  app.get('/admin/:token/users/needConfirm', (req, res) => route.getUsersThatNeedToConfirm(req, res));

  // Get users all users that fully confirmed
  app.get('/admin/:token/users/confirmed', (req, res) => route.getUsersThatConfirmed(req, res));

  // Get medical documents of user's by status
  app.get('/admin/:token/documents', (req, res) => route.getMedicalDocumentsByStatus(req, res));

  // Confirm medical document by id
  app.patch('/admin/:token/updateDocumentStatus/:userID/:documentID', (req, res) => route.updateStatusMedicalDocumentById(req, res));
};
