class Admin {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для подтверждения пользователя после регистрации
   * */
  confirmUserRegister(request, response) {
    this.lib.log.trace('Method "confirmUserRegister" - вызван');
    if (!request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "confirmUserRegister" - параметры прошли валидацию');
    this.helper.searchUserByID(request.query.userID).then(user => {
      if (user.statusRegister === this.helper.ENUM.statusRegister.NOT_CONFIRM.EMAIL)
        response.status(423).send(this.lib.APIMessage.ERROR.USER_NOT_CONFIRMED_EMAIL());
      else if (user.statusRegister === this.helper.ENUM.statusRegister.CONFIRM)
        response.status(202).send(this.lib.APIMessage.CREATOR.createSuccessMessage('Вы уже подвердили данного пользователя'));
      else {
        let documents = user.medicalDocuments.map(document => {
          return {
            img: document.img,
            status: 'verified',
            id: document.id
          }
        });
        let update = {
          $set: {
            statusRegister: this.helper.ENUM.statusRegister.CONFIRM,
            medicalDocuments: documents
          }
        };
        this.lib.db.collection('users').updateOne({id: user.id}, update, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        });
      }
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для деактивации аккаунта пользователя
   * */
  deactivateUser(request, response) {
    this.lib.log.trace('Method "deleteUser" - вызван');
    if (!request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "deleteUser" - параметры прошли валидацию');
    this.lib.db.collection('users').updateOne({id: request.query.userID}, {$set: {statusRegister: this.helper.ENUM.statusRegister.DEACTIVATED}},(err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
    });
  }

  /**
   * Метод для удаления аккаунта пользователя из бд
   * */
  deleteUser(request, response) {
    this.lib.log.trace('Method "deleteUser" - вызван');
    if (!request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "deleteUser" - параметры прошли валидацию');
    this.lib.db.collection('users').findOne({id: request.query.userID}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(412).send(this.lib.APIMessage.ERROR.WRONG_USER_ID());
      else {
        this.lib.db.collection('users').deleteOne({id: request.query.userID}, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else {
            let waitingArray = [];
            // delete user avatar
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.fs.stat(`app/static/avatar/${request.query.userID}.jpg`, (err, status) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else {
                  this.lib.fs.unlink(`app/static/avatar/${request.query.userID}.jpg`, err => {
                    if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                    else resolve();
                  });
                }
              });
            }));

            // remove user's posts
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('posts').deleteMany({userID: request.query.userID}, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            }));

            // remove messages
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('messages').deleteMany({$or: [{to: request.query.userID}, {from: request.query.userID}]}, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            }));

            // delete document's
            waitingArray.push(new Promise((resolve, reject) => {
              require('rimraf')(`app/static/medicalDocuments/${item._id}`, (err) => {
                if (err) reject({status: 500, message: err});
                else resolve();
              });
            }));

            // delete specialist questions
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('specialist-question').deleteMany({owner: {id: request.query.userID}}, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            }));

            // delete specialist question comments
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('specialist-question').find({}).toArray((err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else if (item.length === 0) resolve();
                else {
                  let waiting = [];
                  for (let i = 0; i < item.length; i++) {
                    let comments = item[i].comments;
                    let temp = comments.filter(comment => comment.user.userID !== request.query.userID);
                    if (comments.length !== temp.length) {
                      waiting.push(new Promise((resolve, reject) => {
                        this.lib.db.collection('specialist-question').updateOne({id: this.lib.ObjectID(item[i]._id)}, {$set: {comments: temp}}, (err, item) => {
                          if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                          else resolve();
                        });
                      }));
                    }
                  }

                  Promise.all(waiting).then(() => {
                    resolve();
                  }).catch(err => {
                    reject(err);
                  });
                }
              });
            }));

            // delete user's vacancy
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('Jobs').deleteMany({userID: request.query.userID}, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            }));

            // delete forum's topic
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('forum').deleteMany({'owner.id': request.query.userID}, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            }));

            // delete forum's comment
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('forum').find({}).toArray((err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else if (item.length === 0) reject({status: 204, message: ''});
                else {
                  let waiting = [];
                  for (let i = 0; i < item.length; i++) {
                    let comments = item[i].comments;
                    let filtered = comments.filter(comment => comment.user.userID !== request.query.userID);
                    if (filtered.length !== comments.length) {
                      waiting.push(new Promise((resolve, reject) => {
                        let set = {$set: {comments: filtered}};
                        this.lib.db.collection('forum').updateOne({_id: this.lib.ObjectID(item[i]._id)}, set, (err, item) => {
                          if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                          else resolve();
                        })
                      }));
                    }
                  }

                  Promise.all(waiting).then(() => {
                    resolve();
                  }).catch(err => {
                    reject(err);
                  })
                }
              });
            }));

            // delete user from therapist
            waitingArray.push(new Promise((resolve, reject) => {
              this.lib.db.collection('users').find({}).toArray((err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else {
                  let waiting = [];
                  for (let i = 0; i < item.length; i++) {
                    let therapist = item[i].therapist;
                    if (Array.isArray(therapist)) {
                      let tmp = therapist.filter(usr => usr.userID !== request.query.userID);
                      if (tmp.length !== therapist.length) {
                        waiting.push(new Promise((resolve, reject) => {
                          this.lib.db.collection('users').updateOne({id: item[i].id}, {$set: {therapist: tmp}}, (err, item) => {
                            if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                            else resolve();
                          });
                        }));
                      }
                    } else {
                      if (therapist && therapist.userID === request.query.userID) {
                        waiting.push(new Promise((resolve, reject) => {
                          this.lib.db.collection('users').updateOne({id: item[i].id}, {$set: {therapist: []}}, (err, item) => {
                            if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                            else resolve();
                          });
                        }));
                      }
                    }
                  }

                  Promise.all(waiting).then(() => {
                    resolve();
                  }).catch(err => {
                    reject(err);
                  });
                }
              });
            }));

            Promise.all(waitingArray).then(() => {
              response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
            }).catch(err => {
              response.status(err.status).send(err.message);
            })
          }
        });
      }
    });
  }

  /**
   * Метод для изменения роли пользователя
   * */
  changeUserRole(request, response) {
    this.lib.log.trace('Method "changeUserRole" - вызван');
    if (!request.query.userID || !request.query.role) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "changeUserRole" - параметры прошли валидацию');
    this.helper.searchUserByID(request.query.userID).then(user => {
      if (this.helper.checkForRoleName(request.query.role) && !this.helper.checkForRoleAccess(request.query.role)) {
        response.status(403).send(this.lib.APIMessage.ERROR.WRONG_ROLE_NAME());
        return;
      }

      const newRole = {
        id: this.helper.getRoleIdAllRole(request.query.role),
        name: request.query.role
      };

      this.lib.db.collection('users').updateOne({id: request.query.userID}, {$set: {role: newRole}}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения данных пользователей, котрые полностью подтверждены
   * */
  getUsersThatConfirmed(request, response) {
    this.lib.log.trace('Method "getUsersThatConfirmed" - вызван');
    this.lib.db.collection('users').find({statusRegister: this.helper.ENUM.statusRegister.CONFIRM}).toArray((err, item) => {
      if (err) response.send(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.send(412).send(this.lib.APIMessage.SUCCESS.USER_EMPTY());
      else {
        let users = item.filter(user => !user._id.toString().includes(request.params.token));
        users = users.map(user => this.helper.getBasicUserInfo(user));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    });
  }

  /**
   * Метод для получения данных пользователей, котрые требуют подтверждения админом
   * */
  getUsersThatNeedToConfirm(request, response) {
    this.lib.log.trace('Method "getUsersThatNeedToConfirm" - вызван');
    this.lib.db.collection('users').find({statusRegister: this.helper.ENUM.statusRegister.NOT_CONFIRM.ADMIN}).toArray((err, item) => {
      if (err) response.send(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(412).send(this.lib.APIMessage.CREATOR.createSuccessMessage('Нет пользователей на модерацию'));
      else {
        let users = item.map(user => Object.assign({}, this.helper.getBasicUserInfo(user), {documents: user.medicalDocuments.map(med => `http://${this.helper.apiHost}/document/${user._id}/${med.img}`)}));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    });
  }

  /**
   * Получения информаций о документах пользователей по статусу
   * */
  getMedicalDocumentsByStatus(request, response) {
    this.lib.log.trace('Method "getMedicalDocumentsByUserIdAndStatus" - вызван');
    if (request.query.status !== 'verified' && request.query.status !== 'unverified') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getMedicalDocumentsByUserIdAndStatus" - вызван');
    this.lib.db.collection('users').find({medicalDocuments: {$elemMatch: {status: request.query.status}}}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(202).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      else {
        item = item.filter(user => user._id.toString() !== request.params.token && user.statusRegister === this.helper.ENUM.statusRegister.CONFIRM);
        let users = item.map(user => this.helper.getBasicUserInfo(user));
        for (let i = 0; i < users.length; i++) {
          let userDocuments = item[i].medicalDocuments.filter(document => document.status === request.query.status);
          userDocuments = userDocuments.map(document => {
            return {
              id: document.id,
              img: `http://${this.helper.apiHost}/document/${item[i]._id}/${document.img}`,
              status: document.status
            }
          });
          users[i] = Object.assign({}, users[i], {medicalDocuments: userDocuments});
        }
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    });
  }

  /**
   * Метод для изменения статуса документа по id
   * */
  updateStatusMedicalDocumentById(request, response) {
    this.lib.log.trace('Method "updateStatusMedicalDocumentById" - вызван');
    if (!request.params.userID || !request.params.documentID && (request.query.status !== 'verified' && request.query.status !== 'canceled')) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "updateStatusMedicalDocumentById" - вызван');
    this.lib.db.collection('users').updateOne({id: request.params.userID, "medicalDocuments.id": request.params.documentID}, {$set: {"medicalDocuments.$.status": request.query.status}}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else response.send(this.lib.APIMessage.SUCCESS.MEDICAL_DOCUMENT_VERIFIED_SUCCESS());
    });
  }
}


module.exports = Admin;
