module.exports = (app, lib) => {
  const User = require('./user');
  const route = new User(lib);

  app.use('/user', (req, res, next) => {
    lib.log.info(`URL: /user${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Routing
  // Auth user
  app.get('/user/auth', (req, res) => route.auth(req, res));
  // Auth user with token
  app.get('/user/auth/:token', (req, res) => route.authWithToken(req, res));
  // Register user
  app.post('/user', (req, res) => route.register(req, res));
  // Update user password
  app.patch('/user/updatePassword', (req, res) => route.updatePassword(req, res));
  // Update public information
  app.patch('/user/public', (req, res) => route.updatePublicInformation(req, res));
  // Get public information
  app.get('/user/public', (req, res) => route.getPublicInformation(req, res));
  // Update privacy setting
  app.patch('/user/privacy', (req, res) => route.updatePrivacyInformation(req, res));
  // Get privacy setting
  app.get('/user/privacy/:token', (req, res) => route.getPrivacyInformation(req, res));
  // Get confirm
  app.get('/user/confirmRegister/:id', (req, res) => route.confirmRegistration(req, res));
  //Get reset password link
  app.get('/user/resetPassword/link', (req, res) => route.getResetPasswordLink(req, res));
  // Set new password
  app.patch('/user/resetPassword/:id', (req, res) => route.resetPassword(req, res));
  // Update user status
  app.patch('/user/:token/status', (req, res) => route.updateStatus(req, res));
  // Upload new avatar
  app.post('/user/:token/avatar', (req, res, next) => route.uploadAvatar(req, res, next));
  // Get user's job summary
  app.get('/user/:userId/jobSummary', (req, res) => route.getUserSummaryForJob(req, res));

  // ------------------------ SEARCH USER ----------------------
  app.get('/user/:token/search', (req, res) => route.searchUserThatAllowsFriend(req, res));
  app.get('/user/search', (req, res) => route.searchUser(req, res));

  // ------------------------ MEDICAL DOCUMENTS ----------------------
  // Upload new documents
  app.post('/user/:token/document', (req, res, next) => route.addMedicalDocument(req, res, next));
  // Get all documents
  app.get('/user/:token/document', (req, res) => route.getMedicalDocuments(req, res));
  // Get documents by status
  app.get('/user/:token/document/:status', (req, res) => route.getMedicalDocumentsByStatus(req, res));

  // ------------------------------ PRIVACY --------------------------
  // GET users that allow messages
  app.get('/user/:token/allowChat', (req, res) => route.getUsersThatAllowsMessage(req, res));
  // GET users that allow friend request
  app.get('/user/:token/allowFriend', (req, res) => route.getUsersThatAllowsFriend(req, res));
  // GET all users
  app.get('/users/:token', (req, res) => route.getUsersPublicInformation(req, res));
};
