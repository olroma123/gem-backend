module.exports = (app, lib) => {
  const Jobs = require('./jobs');
  const route = new Jobs(lib);

  app.use('/jobs', (req, res, next) => {
    lib.log.info(`URL: /jobs${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Get all jobs
  app.get('/jobs', (req, res) => route.getJobs(req, res));
  // Add new job
  app.post('/jobs/:token', (req, res) => route.addJob(req, res));
  // Add summary
  app.post('/job/:token/summary', (req, res) => route.addSummary(req, res));
  // Remove job
  app.delete('/job/:token', (req, res) => route.removeJobById(req, res));
  // Get job by name or id
  app.get('/job', (req, res) => route.getJobBy(req, res));
  // Accept job
  app.patch('/job/:token', (req, res) => route.acceptJob(req, res));
  // Get jobs by moderation status
  app.get('/jobs/moderation/:token', (req, res) => route.getModerationJobs(req, res));

  app.get('/jobs/moderation-user/:token', (req, res) => route.getModerationUserJobs(req, res));

  // Get jobs by filter
  app.post('/job-search/filter', (req, res) => route.getJobsByFilter(req, res));
};
