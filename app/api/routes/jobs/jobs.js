class Jobs {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для добавления новой вакансии
   * */
  addJob(request, response) {
    this.lib.log.trace('Method "addJob" - вызван');
    if (!request.params.token || !request.body.job) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addJob" - прошел валидацию параметров');
    let job = JSON.parse(this.lib.stripTags(request.body.job));
    if (!job || !this.helper.validateJob(job)) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.helper.searchUserByToken(request.params.token).then(user => {
      // set status in dependence of user role
      job.status = this.helper.checkForRoleAccess(user.role.name) ? user.role.name !== 'Модератор' ? 'accepted' : 'moderation' : 'moderation';
      job.userID = user.id;

      this.lib.db.collection('Jobs').insertOne(job, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(job.status === 'accepted' ? this.lib.APIMessage.SUCCESS.JOBS_ADD_SUCCESS_ADM() : this.lib.APIMessage.SUCCESS.JOBS_ADD_SUCCESS());
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для добавления резюме пользователя
   * */
  addSummary(request, response) {
    this.lib.log.trace('Method "addSummary" - вызван');
    if (!request.params.token || !request.body.summaryUser || typeof request.body.summaryUser !== 'string') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addSummary" - прошел валидацию параметров');
    let summary = JSON.parse(this.lib.stripTags(request.body.summaryUser));
    if (!summary || !this.helper.validateSummary(summary)) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('users').updateOne({_id: this.lib.ObjectID(request.params.token)}, {$set: {jobSummary: summary}}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для подтверждения одобрения вакансии администратором
   * */
  acceptJob(request, response) {
    this.lib.log.trace('Method "acceptJob" - вызван');
    if (!request.params.token || !request.query.id) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "acceptJob" - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(admin => {
      this.lib.db.collection('Jobs').updateOne({_id: this.lib.ObjectID(request.query.id)}, {$set: {status: 'accepted'}}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения вакансий, которые требуют одобрения
   * */
  getModerationJobs(request, response) {
    this.lib.log.trace('Method "getModerationJobs" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getModerationJobs" - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(admin => {
      let query = [
        {$lookup: {from: "users", localField: "userID", foreignField: "id", as: "userInfo"}},
        {$match: {status: 'moderation'}},
      ];
      this.lib.db.collection('Jobs').aggregate(query).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send();
        else {
          let jobs = [];
          for (let i = 0; i < item.length; i++) {
            item[i].userInfo = {
              fullNameUser: this.helper.getFullName(item[i].userInfo[0].public.fullName),
              avatar: this.helper.getUserAvatarByID(item[i].userInfo[0].id)
            }
          }
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения вакансии со статусом модерирования для пользователя
   * */
  getModerationUserJobs(request, response) {
    this.lib.log.trace('Method "getModerationUserJobs" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getModerationUserJobs" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('Jobs').find({$and: [{userID: user.id}, {status: 'moderation'}]}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        else {
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
        }
      })
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения всех вакансий
   * */
  getJobs(request, response) {
    this.lib.log.trace('Method "getJobs" - вызван');
    this.lib.db.collection('Jobs').find({status: 'accepted'}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204);
      else {
        let jobs = item.map(job => {
          return {
            title: job.title,
            company: job.company,
            price: job.price,
            id: job._id.toString()
          }
        });
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(jobs));
      }
    });
  }

  /**
   * Обобщенный метод для поиска вакансий
   * Вызывает нужные методы в зависимости от того, по какому криттерию надо найти
   * */
  getJobBy(request, response) {
    this.lib.log.trace('Method "getJobBy" - вызван');
    if (!request.query) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getJobBy" - прошел валидацию параметров');
    if (request.query.name)
      this.getJobByName(request, response);
    else if (request.query.id)
      this.getJobById(request, response);
    else response.status(400).send(this.lib.APIMessage.CREATOR.createErrorMessage('Неправильный запрос! Требуется указать id или название вакансии.'));
  }

  /**
   * Метод для поиска вакансии по имени
   * */
  getJobByName(request, response) {
    this.lib.log.trace('Method "getJobByName" - вызван');
    if (!request.query.name) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getJobByName" - прошел валидацию параметров');
    this.lib.db.collection('Jobs').find({title: decodeURIComponent(request.query.name)}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204);
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
    });
  }

  /**
   * Метод поиска вакансии по id
   * */
  getJobById(request, response) {
    this.lib.log.trace('Method "getJobById" - вызван');
    if (!request.query.id) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getJobById" - прошел валидацию параметров');
    this.lib.db.collection('Jobs').find({_id: this.lib.ObjectID(request.query.id)}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.JOBS_EMPTY());
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
    });
  }

  /**
   * Метод получения данных о вакансиях по фильтру
   * */
  getJobsByFilter(request, response) {
    this.lib.log.trace('Method "getJobsByFilter" - вызван');
    let query = {$or: []};
    if (!request.body.filter) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    let filter = this.helper.tryParseJson(request.body.filter);
    if (!filter || !filter.exp || !filter.city) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    // get exp query
    if (filter.exp.includes('любой')) {
      query.$or.push({exp: []});
    } else {
      for (let i = 0; i < filter.exp.length; i++)
        query.$or.push({exp: filter.exp[i]});
    }

    // get city filter
    for (let i = 0; i < filter.city.length; i++)
      query.$or.push({city: {$regex: `.*${filter.city[i]}.*`}});
    if (filter.city.length === 0) query.$or.push({city: []});

    if (query.$or.length > 0) {
      if (query.$or[0].exp === {} || query.$or[0].exp && query.$or[0].exp.length === 0)
        query.$or.splice(0, 1);
      if (query.$or[query.$or.length - 1].city === {} || query.$or[query.$or.length - 1].city && query.$or[query.$or.length - 1].city.length === 0)
        query.$or.splice(query.$or.length - 1, 1);
    }

    if (query.$or.length === 0)
      query = {};

    this.lib.db.collection('Jobs').find(query).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send(this.lib.APIMessage.SUCCESS.JOBS_EMPTY());
      else {
        let jobs = item.filter(job => job.status === 'accepted');

        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(jobs = jobs.map(job => {
          return {
            title: job.title,
            company: job.company,
            price: job.price,
            id: job._id.toString()
          }
        })));
      }
    });
  }

  /**
   * Метод для удаления вакансии по id
   * */
  removeJobById(request, response) {
    this.lib.log.trace('Method "removeJobById" - вызван');
    if (!request.query.id || !request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "removeJobById" - вызван');
    this.helper.searchUserByToken(request.params.token).then(admin => {
      if (!this.helper.checkForRoleAccess(admin.role.name)) {
        response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
        return;
      }

      this.lib.db.collection('Jobs').deleteOne({_id: this.lib.ObjectID(request.query.id)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      });
    }).catch(error => {
      response.status(error.status).send(error.message);
    })
  }
}

module.exports = Jobs;
