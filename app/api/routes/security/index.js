module.exports = (app, lib) => {
  const Security = require('./security');
  const route = new Security(lib);

  // Validate user object
  app.post('/security/:token', (req, res) => route.validateHashUser(req, res));
  // Update item in documents
  app.patch('/security/db/:token', (req, res) => route.insertFieldToAllDocuments(req, res));
  // Is user have an admin rights
  app.get('/security/:token/admin', (req, res) => route.isUserHaveAdminRights(req, res));

  // Checking for specialist user role
  app.get('/security/:token/doctor', (req, res) => route.isUserIsSpecialist(req, res));
};
