class Security {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для проверки соответствия полученных данных от данных, которые находятся в бд
   * */
  validateHashUser(request, response) {
    this.lib.log.trace('Method "validateHashUser" - вызыван');
    if (!request.body.user || !request.params.token || typeof request.body.user !== 'string') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    let user = JSON.parse(request.body.user);
    if (user._id !== request.params.token) {
      response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      return;
    }
    this.lib.log.trace('Method "validateHashUser" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let originalHash = JSON.stringify(this.helper.getAuthUserInfo(user));
      let transferedHash = JSON.stringify(user);
      if (originalHash === transferedHash)
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      else response.status(302).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(this.helper.getAuthUserInfo(user)));
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для проверки роли пользователя (администраторские права)
   * */
  isUserHaveAdminRights(request, response) {
    this.lib.log.trace('Method "isUserHaveAdminRights" - вызыван');
    if (!request.params.token || request.params.token === 'undefined') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "isUserHaveAdminRights" - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
    }).catch(err => {
      response.status(err.status || 500).send(err.message || this.lib.APIMessage.ERROR.SERVER_ERROR());
    });
  }

  /**
   * Метод для проверки роли пользователя (специалист)
   * */
  isUserIsSpecialist(request, response) {
    this.lib.log.trace('Method "isUserIsSpecialist" - вызыван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "isUserIsSpecialist" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      response.status(user.role.name === 'Специалист-консультант' ? 200 : 403).send();
    }).catch(err => {
      response.status(err.status || 500).send(err.message || this.lib.APIMessage.ERROR.SERVER_ERROR());
    });
  }

  /**
   * Мусорный метод. Нужен был для того, чтобы быстро добавлять нужные поля в бд
   * */
  insertFieldToAllDocuments(request, response) {
    this.lib.log.trace('Method "insertFieldToAllDocuments" - вызыван');
    if (!request.params.token || !request.query.collection) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "insertFieldToAllDocuments" - прошел валидацию параметров');
    this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.token)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null || !this.helper.checkForRoleAccess(item.role.name)) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        this.lib.log.trace('Method "insertFieldToAllDocuments" - валидация администратора прошла успешно');
        this.lib.db.collection(request.query.collection).find({}).toArray((err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          else {
            for (let i = 0; i < item.length; i++)
              this.lib.db.collection(request.query.collection).update({_id: this.lib.ObjectID(item[i]._id)}, {$set: {statusRegister: 2}});
          }
        });
      }
    });
  }
}

module.exports = Security;
