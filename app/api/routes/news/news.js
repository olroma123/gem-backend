class News {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
    this.validateNews = (newsObject) => {
      if (!newsObject) return false;
      if (!newsObject.date || !newsObject.text || !newsObject.previewText || !newsObject.title) return false;
      if (newsObject.visibility !== 'all' && newsObject.visibility !== 'registerOnly') return false;
      if (new Date(newsObject.date) == 'Invalid date') return false;
      return true;
    }
  }

  /**
   * Метод для добавления новости
   * */
  addNews(request, response) {
    this.lib.log.trace('Method "addNews" - вызыван');
    this.lib.log.trace('Method "addNews" - прошел валидацию параметров');

    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      const ID = this.lib.ObjectID();
      const storage = this.lib.multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, `./app/static/img/news/`)
        },

        filename: (req, file, cb) => {
          if ((file.mimetype.includes('jpeg') || file.mimetype.includes('jpg') || file.mimetype.includes('png')) && req.params.token) {
            let fileName = ID.toString();

            if (!this.lib.fs.existsSync(`./app/static/img/news/`))
              this.lib.fs.mkdirSync(`./app/static/img/news/`);

            cb(null, `${fileName}.jpg`);
          } else cb(this.lib.APIMessage.ERROR.WRONG_FILE_EXT('jpg, jpeg, png'), false);
        }
      });

      const upload = this.lib.multer({storage: storage, limits: { fileSize: 3145728 }}).single('image');
      upload(request, response, (err) => {
        if (err) {
          if (err.code === 'LIMIT_FILE_SIZE') response.send(this.lib.APIMessage.ERROR.WRONG_FILE_SIZE());
          else response.status(400).send(err);
        } else {
          if (!request.params.token || !request.body.news || !request.body.image) {
            response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
            return;
          }

          const news = this.helper.tryParseJson(this.lib.stripTags(request.body.news));
          if (!news || !this.validateNews(news)) {
            response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
            return;
          }

          news._id = ID;
          news.date = new Date(news.date);
          news.comments = [];
          this.lib.db.collection('news').insertOne(news, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else {
              if (typeof request.body.image === 'string') {
                this.lib.base64Image.img(request.body.image, './app/static/img/news/', item.ops[0]._id, (err, filepath) => {
                  if (err) response.status(400).send(this.lib.APIMessage.ERROR.WRONG_IMAGE());
                  else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id}));
                });
              } else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id}));
            }
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения новостных миниатюр
   * */
  getPreviewNews(request, response) {
    // If we have an offset
    // Than send only news in that offset
    if (request.query.offset && request.query.offset >= 0 && request.query.count && request.query.count) {
      this.lib.db.collection('news').aggregate([]).sort({'date': 1}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send(this.lib.APIMessage.SUCCESS.NEWS_EMPTY());
        else {
          let previewNews = item.map(news => {
            return {
              text: news.previewText,
              date: news.date,
              title: news.title,
              link: `/news/${news._id.toString()}`,
              visibility: news.visibility,
              img: `http://${this.helper.apiHost}/images/news/${news._id.toString()}.jpg`
            }
          });

          let count = 0, news = [];
          for (let i = request.query.offset; i < previewNews.length; i++) {
            if (count === +request.query.count) break;
            news.push(previewNews[i]);
            count++;
          }

          if (request.query.userID) {
            this.helper.searchUserByID(request.query.userID).then(user => {
              response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(news));
            }).catch(err => {
              response.status(err.status).send(err.message);
            });
          } else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(news.filter(newsObject => newsObject.visibility === 'all')));
        }
      });
    }else if (request.query.count && request.query.count > 0) {
      // If we have a count query
      // Than send only "count" news
      this.lib.db.collection('news').aggregate([{$limit: +request.query.count}]).sort({'date': 1}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.NEWS_EMPTY());
        else {
          let previewNews = item.map(news => {
            return {
              text: news.previewText,
              date: news.date,
              title: news.title,
              link: `/news/${news._id.toString()}`,
              visibility: news.visibility,
              img: `http://${this.helper.apiHost}/images/news/${news._id.toString()}.jpg`
            }
          });

          if (request.query.userID) {
            this.helper.searchUserByID(request.query.userID).then(user => {
              response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(previewNews));
            }).catch(err => {
              response.status(err.status).send(err.message);
            });
          } else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(previewNews.filter(newsObject => newsObject.visibility === 'all')));

        }
      });
    } else {
      // If we not have a limit query
      // Than we just send all news
      this.lib.db.collection('news').aggregate([]).sort({'date': 1}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.NEWS_EMPTY());
        else {
          let previewNews = item.map(news => {
            return {
              text: news.previewText,
              date: news.date,
              title: news.title,
              link: `/news/${news._id.toString()}`,
              visibility: news.visibility,
              img: `http://${this.helper.apiHost}/images/news/${news._id.toString()}.jpg`
            }
          });

          if (request.query.userID) {
            this.helper.searchUserByID(request.query.userID).then(user => {
              response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(previewNews));
            }).catch(err => {
              response.status(err.status).send(err.message);
            });
          } else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(previewNews.filter(newsObject => newsObject.visibility === 'all')));
        }
      });
    }
  }

  /**
   * Метод для получения подробной информаци о новости по его ID
   * */
  getNewsById(request, response) {
    this.lib.log.trace('Method "getNewsById" - вызыван');
    if (!request.query.id) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getNewsById" - прошел валидацию параметров');
    this.lib.db.collection('news').findOne({_id: this.lib.ObjectID(request.query.id)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null) response.status(412).send(this.lib.APIMessage.SUCCESS.NEWS_EMPTY());
      else {
        item.img = `http://${this.helper.apiHost}/images/news/${item._id.toString()}.jpg`;
        if (request.query.userID) {
          this.helper.searchUserByID(request.query.userID).then(user => {
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
          }).catch(err => {
            response.status(err.status).send(err.message);
          });
        } else {
          if (item.visibility === 'all') response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
          else response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
        }
      }
    });
  }

  /**
   * Метод для поиски новости по его title
   * */
  searchNews(request, response) {
    this.lib.log.trace('Method "searchNews" - вызван');
    this.lib.db.collection('news').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else {
        let news = item.filter(news => news.title.toLowerCase().includes(decodeURIComponent(request.query.search.toLowerCase() || '')));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(news));
      }
    });
  }

  /**
   * Метод для добавления комментария к новостям
  */
  addCommentToNews(request, response) {
    this.lib.log.trace('Method "addCommentToNews" - вызван');
    if (!request.params.token || !request.body.text || !request.query.newsId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addCommentToNews" - прошел валидацию параметров');
    request.body.text = this.lib.stripTags(request.body.text);
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('news').findOne({_id: this.lib.ObjectID(request.query.newsId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(406).send(this.lib.APIMessage.ERROR.WRONG_NEWS_ID());
        else {
          let comments = item.comments || [];
          comments.push({
            user: {
              id: user.id,
              fullName: this.helper.getShortFullName(user.public.fullName)
            },
            text: request.body.text,
            date: new Date().toISOString()
          });

          this.lib.db.collection('news').updateOne({_id: this.lib.ObjectID(request.query.newsId)}, {$set: {comments: comments}}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = News;
