class Friend {
  constructor(lib) {
    this.lib = lib.lib;
    this.lib.cronLogger = lib.cronLogger;
    this.helper = lib.helper;
    this.DateDiff = require('date-diff');
  }

  alertUserOnRemindBeforeDay() {
    this.lib.db.collection('users').find({}).toArray((err, item) => {
      if (err) this.lib.cronLogger.error(err);
      else {
        for (let user of item) {
          let userRemindsNotPlan = user.reminds.filter(remind => !remind.isPlan);
          let userRemindsPlan = user.reminds.filter(remind => remind.isPlan);

          for (let remind of userRemindsNotPlan) {
            let remindDate = remind.date.split('.');
            remindDate = new Date(+remindDate[2], +remindDate[1] - 1, +remindDate[0]);
            let now = new Date();
            let diff = new this.DateDiff(now, remindDate);

            // If remind in not already alerted
            // Than we need to alert user
            if (diff.days() <= 1 && diff.days() >= -1 && !remind.isAlerted) {
              // set isAlerted status to db
              this.lib.db.collection('users').updateOne({id: user.id, "reminds.id": remind.id}, {$set: {"reminds.$.isAlerted": true}}, (err, item) => {
                if (err) this.lib.cronLogger.error(err);
                else {
                  // send to email
                  this.lib.email.send(this.lib.emailTemplate.alertUserRemind(user.email, remind.text), (err, mess) => {
                    if (err) this.lib.cronLogger.error(err);
                    else this.lib.cronLogger.trace(`User ${user.id} is alerted by remind id - ${remind.id}`);
                  });
                }
              });
            }
          }
        }
      }
    });
  }
}

module.exports = Friend;
