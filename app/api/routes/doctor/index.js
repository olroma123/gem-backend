module.exports = (app, lib) => {
  const Doctor = require('./doctor');
  const route = new Doctor(lib);

  app.use('/doctor', (req, res, next) => {
    lib.log.info(`URL: /doctor${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // add therapist to user
  app.post('/doctor/:token', (req, res) => route.addTherapist(req, res));

  // get therapist by status
  app.get('/doctor/status/:token', (req, res) => route.getTherapistByStatus(req, res));

  app.get('/doctors/:token', (req, res) => route.getListOfDoctors(req, res));

  // accept user to therapist
  app.patch('/doctor/accept/:token', (req, res) => route.acceptTherapist(req, res));

  // Send message to doctor or user
  app.post('/doctor/message/:token', (req, res) => route.sendMessage(req, res));

  // Get a messages from doctor or user
  app.get('/doctor/message/:token', (req, res) => route.getMessages(req, res));

  // Add new reminds to user
  app.post('/doctor/remind/:token', (req, res) => route.addRemindToUser(req, res));

  // Get remind's plan of user
  app.get('/doctor/remind/:token', (req, res) => route.getRemindsOfUser(req, res));

  app.get('/doctor/user-status/:token', (req, res) => route.getUserStatusTherapist(req, res));
};
