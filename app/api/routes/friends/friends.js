class Friend {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для отправки запроса на добавления в друзья
   * */
  addFriend(request, response) {
    this.lib.log.trace('Method "addFriend" - вызыван');
    if (!request.params.token || !request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addFriend" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.log.trace('Method "addFriend" - пользователь прошел валидацию по токену');
      this.helper.searchUserByID(request.params.userID).then(queryUser => {
        this.lib.log.trace('Method "addFriend" - добавляемый пользователь был найден в бд');
        if (queryUser.privacy.allowFriendRequest === 'false') {
          response.status(403).send(this.lib.APIMessage.ERROR.PRIVACY_NOT_ALLOW());
          this.lib.log.trace('Method "addFriend" - отклонено из-за настроек приватности');
          return;
        }

        let query = {
          $or: [
            {
              user1: this.lib.ObjectID(request.params.token),
              user2: this.lib.ObjectID(queryUser._id)
            },
            {
              user2: this.lib.ObjectID(request.params.token),
              user1: this.lib.ObjectID(queryUser._id)
            }
          ]
        };
        this.lib.db.collection('friends').findOne(query, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else if (item !== null) response.status(412).send(this.lib.APIMessage.ERROR.ALREADY_FRIEND());
          else {
            let friendRequest = {user1: this.lib.ObjectID(request.params.token), user2: queryUser._id, status: 'requested'};
            this.lib.db.collection('friends').insertOne(friendRequest, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
              else response.send(this.lib.APIMessage.SUCCESS.FRIEND_REQUEST_SUCCESS());
            });
          }
        });
      }).catch(err => {
        response.status(err.status || 500).send(err.message);
      });
    }).catch(err => {
      response.status(err.status || 500).send(err.message);
    });
  }

  /**
   * Метод для получения всех друзей не зависимо от того, принял запрос или нет
   * */
  getFriends(request, response) {
    this.lib.log.trace('Method "getFriends" - вызыван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    let friends = [
      {$lookup: {from: "users", localField: "user2", foreignField: "_id", as: "userInfo"}},
      {$match: {user1: this.lib.ObjectID(request.params.token)}},
    ];
    this.lib.log.trace('Method "getFriends" - прошел валидацию параметров');
    this.lib.db.collection('friends').aggregate(friends).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null || item.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.FRIENDS_EMPTY());
      else {
        // filter deleted user (userInfo should have length 0)
        // TODO: Need delete this user from db
        let filteredFriends = item.filter(user => user.userInfo.length !== 0);

        // Than copy friend info
        let friend = filteredFriends.map(friendInfo => Object.assign({}, this.helper.getBasicUserInfo(friendInfo.userInfo[0]), {status: friendInfo.status}));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(friend));
      }
    });
  }

  /**
   * Метод для получения друзей, который приняли запрос на добавление
   * */
  getAcceptedFriends(request, response) {
    this.lib.log.trace('Method "getAcceptedFriends" - вызыван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getAcceptedFriends" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let acceptedFriendQuery = [
        {$lookup: {from: 'users', localField: 'user2', foreignField: '_id', as: 'userInfo'}},
        {$match: {$and: [{user1: this.lib.ObjectID(request.params.token)}, {status: 'accepted'}]}},
        {$group: { "_id": "$_id", user: {$last: "$userInfo"}}}
      ];
      this.lib.db.collection('friends').aggregate(acceptedFriendQuery).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item.length === 0) response.status(410).send(this.lib.APIMessage.SUCCESS.FRIENDS_EMPTY());
        else {
          item = item.filter(user => user.user.length > 0);
          let friends = item.map(user => this.helper.getBasicUserInfo(user.user[0]));
          response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({count: friends.length, friends: friends}));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для полученя заявок на добавления друзей
   * */
  getPendingFriendRequest(request, response) {
    this.lib.log.trace('Method "getPendingFriendRequest" - вызыван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getPendingFriendRequest" - прошел валидацию параметров');

    let requestedFriendQuery = [
      {$lookup: {from: 'users', localField: 'user1', foreignField: '_id', as: 'userInfo'}},
      {$match: {$and: [{user2: this.lib.ObjectID(request.params.token)}, {status: "requested"}]}},
      { $group: { "_id": "$_id", user: {$last: "$userInfo"}}}
    ];
    this.lib.db.collection('friends').aggregate(requestedFriendQuery).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item.length === 0) response.status(410).send(this.lib.APIMessage.SUCCESS.FRIEND_REQUEST_EMPTY());
      else {
        let friends = item.map(user => this.helper.getBasicUserInfo(user.user[0]));
        response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(friends));
      }
    });
  }

  /**
   * Метод для принятия заявки на добавление в друзья
   * */
  acceptFriend(request, response) {
    this.lib.log.trace('Method "acceptFriend" - вызыван');
    if (!request.params.token || !request.params.friendID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "acceptFriend" - прошел валидацию параметров');
    this.helper.searchUserByID(request.params.friendID).then(user => {
      let userID = user._id;
      let search = {$and: [{user1: this.lib.ObjectID(userID)}, {user2: this.lib.ObjectID(request.params.token)}, {status: 'requested'}]};
      let setStatus = {$set: {status: 'accepted'}};
      this.lib.db.collection('friends').findOneAndUpdate(search, setStatus, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (item === null || item.lastErrorObject.n === 0) response.status(410).send(this.lib.APIMessage.ERROR.IS_NOT_REQUESTED_FOR_FRIEND());
        else {
          this.lib.db.collection('friends').insertOne({user1: this.lib.ObjectID(request.params.token), user2: userID, status: 'accepted'}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else response.send(this.lib.APIMessage.SUCCESS.FRIEND_REQUEST_ACCEPT());
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для удаления пользователя из друзей
   * */
  removeFriend(request, response) {
    this.lib.log.trace('Method "removeFriend" - вызыван');
    if (!request.params.token || !request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace('Method "removeFriend" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.helper.searchUserByID(request.params.userID).then(friend => {
        const token = {
          user: user._id,
          friend: friend._id
        };

        this.lib.db.collection('friends').deleteOne({$and: [{user1: token.user}, {user2: token.friend}]}, (err, result) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else {
            this.lib.db.collection('friends').deleteOne({$and: [{user2: token.user}, {user1: token.friend}]}, (err, result) => {
              if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
              else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
            });
          }
        });
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для проверки является ли пользователи друзьями
   * */
  isUserFriend(request, response) {
    this.lib.log.trace('Method "isUserFriend" - вызыван');
    if (!request.params.token || !request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "isUserFriend" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('users').findOne({id: request.params.userID}, (err, userSecond) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else if (userSecond === null) response.status(412).send(this.lib.APIMessage.CREATOR.createErrorMessage('Данного пользователя не существует'));
        else {
          this.lib.db.collection('friends').findOne({$and: [{user1: user._id}, {user2: userSecond._id}]}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else if (item === null) response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE('not-friend'));
            else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item.status));
          });
        }
      });

    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }
}

module.exports = Friend;
