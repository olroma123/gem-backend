module.exports = (app, lib) => {
  const Friend = require('./friends');
  const route = new Friend(lib);

  app.use('/friend', (req, res, next) => {
    lib.log.info(`URL: /friend${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Add user to friend
  app.post('/friend/:token/add/:userID', (req, res) => route.addFriend(req, res));
  // Get friends
  app.get('/friend/:token', (req, res) => route.getFriends(req, res));
  // Accept friend
  app.patch('/friend/:token/accept/:friendID', (req, res) => route.acceptFriend(req, res));
  // Get accepted friends
  app.get('/friend/:token/accepted', (req, res) => route.getAcceptedFriends(req, res));
  // Get requested friend
  app.get('/friend/:token/requested', (req, res) => route.getPendingFriendRequest(req, res));
  // Is user is friend with another user
  app.get('/friend/:token/isFriend/:userID', (req, res) => route.isUserFriend(req, res));
  // Remove friend from friends list
  app.delete('/friend/:token/:userID', (req, res) => route.removeFriend(req, res));
};
