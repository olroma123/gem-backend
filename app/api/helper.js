class Helper {
  constructor(lib) {
    this.roles = {
      public: [],
      restrict: [],
    };
    this.reminds = {
      types: []
    };
    this.ENUM = {
      statusRegister: {
        CONFIRM: 2,
        NOT_CONFIRM: {
          EMAIL: 0,
          ADMIN: 1
        },
        DEACTIVATED: -1
      }
    };
    this.lib = lib;
    this.socket = {
      log: require('log4js').getLogger('SOCKET'),
      clients: []
    };
    this.ip = this.lib.config.setting.api.host;
    this.apiHost = this.lib.config.isProduction ? `${this.ip}` : `${this.ip}:${this.lib.config.setting.server.port}`;
    this.deepForEach = require('./deepForEach');
    this.lib.log.info(`HOST API -> http://${this.apiHost}`);

    // Initial socket connection
    this.lib.io.on('connection', socket => {
      this.socket.log.info('Подключен новый клиент.');
      this.socket.clients.push({socketID: socket.id, token: socket.handshake.query.token});

      // Listen disconnect event
      socket.on('disconnect', () => {
        this.socket.log.info('Клиент завершил соединение.');
        const index = this.socket.clients.indexOf(socket.id);
        this.socket.clients.splice(index, 1);
      });
    });

    // Get roles name from server
    this.lib.db.collection('Roles').find({}).toArray((err, result) => {
      if (err) {
        this.lib.log.fatal(`Произошла ошибка при получении ролей: ${err}`);
        return;
      }

      for(let i = 0; i < result.length; i++) {
        if (result[i].isTranslated) this.roles.public.push(result[i].text);
        else this.roles.restrict.push(result[i].text);
      }
      this.lib.log.info('Выборка ролей произошла успешно!');
    });

    // Get type of reminds from server
    this.lib.db.collection('remindType').find({}).toArray((err, result) => {
      if (err) {
        this.lib.log.fatal(`Произошла ошибка при выборке типов напоминаний! Текст ошибки ${err}`);
        return;
      }

      this.reminds.types = result.map(remind => {return remind.type});
      this.lib.log.info('Выборка типов напоминаний произошла успешно!');
    });
  }

  getRoleID(name) {
    return this.roles.public.indexOf(name);
  }

  getRoleIdAllRole(name) {
    switch (name) {
      case 'Пользователь': return 0;
      case 'Специалист-консультант': return 1;
      case 'Модератор': return 2;
      case 'Администратор': return 3;
    }
  }

  /**
   * Метод для проверки парвильности названия роли
   * @param {string} name Название роли
   * @return {boolean} Возвращает true, если название роли неправильное
   * */
  checkForRoleName(name) {
    for (let i = 0; i < this.roles.public.length; i++) {
      if (this.roles.public[i] === name)
        return false;
    }
    return true;
  }

  /**
   * Метод, который проверяет название роли на безопасность
   * @param {string} name Название роли
   * @return {boolean} Возвращает true, если роль запрещена для регистрации
   * */
  checkForRoleAccess(name) {
    for (let i = 0; i < this.roles.restrict.length; i++) {
      if (this.roles.restrict[i] === name)
        return true;
    }
    return false;
  }

  /**
   * Метод валидации пользвателя через его токен
   * @param {Object} userCredits Данные пользвателя
   * @param {string} userCredits.login Логин пользователя
   * @param {string} userCredits.token ID пользователя
   * */
  validateUserByToken(userCredits) {
    let user = {
      login: userCredits.login,
      '_id': new this.lib.ObjectID(userCredits.token)
    };

    return new Promise((resolve, reject) => {
      this.lib.db.collection('users').findOne(user, (err, item) => {
        this.lib.log.trace(`Call validateUserByToken: пользователь ${item === null ? 'не прошел': 'прошел'} проверку на валидацию`);
        item === null ? reject() : resolve(item);
      });
    });
  }

  /**
   * Метод для валидации объекта вакансии
   * @return {boolean} Возвращает true, если объект валидный
   * */
  validateJob(job) {
    return job.title && job.company && job.address && job.text && job.contacts && job.city && job.exp && job.price
  }

  validateSummary(summary) {
    return (summary.gender && (summary.gender === 'Мужской' || summary.gender === 'Женский')) && summary.phone && summary.city && summary.birth && summary.nationality
      && summary.education && this.validateEducation(summary.education) && summary.language && this.validateSummaryLanguage(summary.language);
  }

  validateEducation(education) {
    if (!Array.isArray(education)) return false;
    if (education.length > 5 || education.length === 0) return false;
    for (let i = 0; i < education.length; i++) {
      if (!education[i].level || !education[i].name || !education[i].specialization ||!education[i].yearOfEnd) return false;
      if (!Number.isInteger(+education[i].yearOfEnd)) return false;
      if (+education[i].yearOfEnd <= 0 || +education[i].yearOfEnd > new Date().getFullYear() + 50) return false;
      if (education[i].level !== 'Высшее' && education[i].level !== 'Неоконченное высшее' &&
        education[i].level !== 'Среднее специальное' && education[i].level !== 'Среднее') return false;
    }
    return true;
  }

  validateSummaryLanguage(language) {
    if (!Array.isArray(language)) return false;
    if (language.length > 25 || language.length === 0) return false;
    if (!language[0].isPrimary) return false;
    for (let i = 1; i < language.length; i++) {
        if (!language[i].name || !language[i].level) return false;
        if (language[i].level !== 'Базовые знания' && language[i].level !== 'Читаю профессиональную литературу' &&
          language[i].level !== 'Могу свободно проводить интервью' && language[i].level !== 'Свободно владею') return false;
    }
    return true;
  }

  /**
   * Метод валидации пользвателя через его токен
   * @param {string} token Токен пользвателя
   * */
  searchUserByToken(token) {
    return new Promise((resolve, reject) => {
      if (!token) reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()});
      
      this.lib.db.collection('users').findOne({'_id': new this.lib.ObjectID(token)}, (err, item) => {
        this.lib.log.trace(`Call searchUserByToken: пользователь ${item === null ? 'не': ''} найден`);
        if (err) reject({status: 500, message: err});
        else if (item === null) {
          reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()})
        } else {
          if (item.statusRegister !== this.ENUM.statusRegister.CONFIRM) {
            if (item.statusRegister === this.ENUM.statusRegister.NOT_CONFIRM.EMAIL)
              reject({status: 423, message:this.lib.APIMessage.ERROR.AUTH_NOT_CONFIRMED_EMAIL()});
            else if (item.statusRegister === this.ENUM.statusRegister.NOT_CONFIRM.ADMIN)
              reject({status: 423, message:this.lib.APIMessage.ERROR.AUTH_NOT_CONFIRMED_ADMIN()});
            else
              reject({status: 423, message:this.lib.APIMessage.ERROR.USER_DEACTIVATED()});
          }
          else resolve(item);
        }
      });
    });
  }

  /**
   * Метод для валидации пользователя через его токен, при-этом проверяется права админа
   * */
  searchUserByTokenAndValidateAdminRights(token) {
    return new Promise((resolve, reject) => {
      if (!token) reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()});

      this.searchUserByToken(token).then(admin => {
        if (!this.checkForRoleAccess(admin.role.name)) reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()});
        else resolve(admin);
      }).catch(err => {
        reject({status: err.status, message: err.message});
      });
    });
  }

  /**
   * Метод для валидации пользователя через его токен, при-этом проверяется права админа
   * */
  searchUserByTokenAndValidateSpecialistRights(token) {
    return new Promise((resolve, reject) => {
      if (!token) reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()});

      this.searchUserByToken(token).then(specialist => {
        if (specialist.role.name !== 'Специалист-консультант')
          reject({status: 403, message: this.lib.APIMessage.ERROR.USER_IS_NOT_SPECIALIST()});
        else resolve(specialist);
      }).catch(err => {
        reject({status: err.status, message: err.message});
      });
    });
  }

  /**
   * Метод для валидации пользователя по роли и по токену
   * */
  searchUserByTokenAndValidateRole(token, role) {
    return new Promise((resolve, reject) => {
      this.searchUserByToken(token).then(user => {
        if (user.role !== role) reject({status: 403, message: 'Не подходящая роль'});
        else resolve(user);
      }).catch(err => {
        reject(err);
      });
    });
  }

  /**
   * Метод валидации пользвателя через его id
   * @param {string} id ID пользвателя
   * */
  searchUserByID(id) {
    return new Promise((resolve, reject) => {
      if (!id) reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()});
      this.lib.db.collection('users').findOne({id: id}, (err, item) => {
        if (err) reject({status: 500, message: err});
        this.lib.log.trace(`Call searchUserByToken: пользователь ${item === null ? 'не': ''} найден`);
        item === null ? reject({status: 403, message: this.lib.APIMessage.ERROR.ACCESS_DENIED()}) : resolve(item);
      });
    });
  }

  /**
   * Метод для проверки существующего пользователя
   * @param {Object} userCreditional Объект с данными пользователя
   * @param {string} userCreditional.login Логин пользователя
   * @param {string} userCreditional.email Пароль пользователя (в зашифрованном виде)
   * @return {Promise} Возвращает данные о пользователе, если пользователь был найден в бд
   * */
  isUserExists(userCreditional) {
    return new Promise((resolve, reject) => {
      this.lib.db.collection('users').findOne({$or: [{login: userCreditional.login}, {email: userCreditional.email}]}, (err, item) => {
        this.lib.log.trace(`Call isUserExists: пользователь ${item === null ? 'не был': 'был'} найден в бд`);
        item === null ? reject() : resolve(item);
      });
    });
  }

  /**
   * Метод для получения ФИО пользователя через объект
   * @param {Object} userFullNameObj Объект с информации о ФИО
   * @param {string} userFullNameObj.surname Фамилия
   * @param {string} userFullNameObj.name Имя
   * @param {string} userFullNameObj.lastname Отчество
   * */
  getFullName(userFullNameObj) {
    return ((userFullNameObj.surname || '') + ' ' + (userFullNameObj.name || '') + ' ' + (userFullNameObj.lastname || '')).trim();
  }

  /**
   * Метод для получения Фамилии и Имени пользователя через объект
   * @param {Object} userFullNameObj Объект с информации о ФИО
   * @param {string} userFullNameObj.surname Фамилия
   * @param {string} userFullNameObj.name Имя
   * */
  getShortFullName(userFullNameObj) {
    return ((userFullNameObj.surname || '') + ' ' + (userFullNameObj.name || '')).trim();
  }

  /**
   * Метод для получения основной информации о пользователе для предоставления ее другим пользователям
   * */
  getBasicUserInfo(user) {
    return {
      fullName: this.getFullName(user.public.fullName),
      city: user.public.city,
      hemoStatus: user.public.hemoStatus,
      role: user.role.name,
      avatar: this.getUserAvatarByID(user.id),
      id: user.id
    }
  }

  /**
   * Метод для получения основной информации для авторизированного пользователя
   * */
  getAuthUserInfo(user) {
    let obj = {
      _id: user._id,
      id: user.id,
      login: user.login,
      email: user.email,
      role: user.role.name,
      public: user.public,
      hemoStatus: user.public.hemoStatus,
      privacy: user.privacy,
      phone: user.phone,
      avatar: this.getUserAvatar(user)
    };

    if (user.role.name === 'Специалист-консультант')
      obj.roleSpecialist = user.role.specialist;

    return obj;
  }

  /**
   * Метод для получения аватарки пользователя
   * @param {object} user Информация о пользователе
   * */
  getUserAvatar(user) {
    return `http://${this.apiHost}/avatar/${user.id}.jpg`;
  }

  /**
   * Метод для получения аватарки пользователя по его id
   * @param {string} userID ID пользователя
   * */
  getUserAvatarByID(userID) {
    return `http://${this.apiHost}/avatar/${userID}.jpg`;
  }

  /**
   * Метод для отправки сообщение через сокеты
   * @param {string} token Токен пользователя
   * @param {object} data Информация о сообщении
   * @param {string} data.event Название события, которое должен принять клиент
   * @param {object} data.message То, что нужно передать
   * */
  sendDataToSocketID(token, data) {
    if (!token || !data) return;
    let socket = this.socket.clients.find(socket => socket.token == token);
    this.lib.log.trace(`Метод "sendDataToSocketID" - сокет: ${socket ? 'найден' : 'не найден'}`);
    if (socket) this.lib.io.to(socket.socketID).emit(data.event, data.message);
  }

  /**
   * Метод для опредления статуса подтвержденности пользователя
   * */
  userIsNotConfirmed(user) {
    return user.statusRegister === this.ENUM.statusRegister.NOT_CONFIRM.ADMIN ||
      user.statusRegister === this.ENUM.statusRegister.NOT_CONFIRM.EMAIL ||
      user.statusRegister === this.ENUM.statusRegister.DEACTIVATED
  }

  /**
   * Метод для парса JSON строки, которая проверяет го на валдиность
   * @param {string} str Строка в JSON формате
   * @return {JSON | undefined} Возвращает объект с JSON или undefined, если обхект не валиден
   * */
  tryParseJson(str) {
    try {
      let json = JSON.parse(str);
      return json;
    } catch(e) {
      return undefined;
    }
  }
}

module.exports = Helper;
