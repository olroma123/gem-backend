class MailTemplate {
  static registerTemplate(to, confirmLink) {
    return this.createEmail(
      to,
      `Перейдите по ссылке, чтобы подтвердить адрес электронной почты: ${confirmLink}`,
      'Gemofilik'
    );
  }

  static resetPasswordTemplate(to, resetLink) {
    return this.createEmail(
      to,
      `Перейдите по ссылке, чтобы сбросить пароль: ${resetLink}`,
      'Gemofilik'
    );
  }

  static alertUserRemind(to, remindText) {
    return this.createEmail(
      to,
      `Уведомляем вас, что остался 1 день до вашего напоминания: ${remindText}`,
      'Gemonet - напоминание'
    );
  }

  static createEmail(to, text, subject) {
    return {
      text: text,
      to: `<${to}>`,
      from: '<gemofilik-noreply@mail.ru>',
      subject: subject
    };
  }
}

module.exports = MailTemplate;